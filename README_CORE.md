# FN_Risk

Fractured Nations: Risk - Client Side Javascript Tutorial

This project is for my kid and my brother to teach them how to use Javascript. I am making it public for anyone else who would like to learn as well.

We will be building a Risk Game with Node JS. When it is complete you should have a good idea how to use these concepts:

- GIT
- Node JS
- Node Package Manager (NPM)
- Javascript ES6
    - Simple Types
    - Loops
    - Arrays
    - Promises

- Libraries
    - React
    - Redux
    - Express
    - Socket IO

- Concepts
    - Databases(MySql)
    - REST
    - Sockets



