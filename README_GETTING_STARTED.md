# Getting Started

You are going to need to install some stuff before you write your first line of code. At any new job you will need to get familear with setting up a new machine. Here are the things you will need.

## Code Editor

- Install a code editor of some sort.
  There are many out there. I prefer VSCode. Some prefer Webstorm. Others Submlime. Some people even prefer VIM. It should be one you like, you will spend a lot of time there. Try a few.

## Node js

- Install node js
  https://nodejs.org/en/

## Yarn Install

    npm install --global yarn

## Checkout the project

    git clone https://gitlab.com/mardok_42/fn_risk.git

## Install Packages

    yarn

## GIT

- Install git command line
  GIT is a way to keep track of your code. It is a repository management system. A code repository is where the code lives. You can pull it down to your machine. Or push it up to the repository.
  You can find the install instructions here:
  https://git-scm.com/book/en/v2/Getting-Started-Installing-Git

  ## Changing Code

  Here is the typical flow for changing code on a repository. As an example we are going to say you need to make a change to a web sites home page. You do not have the project checked out so the first thing you will do is check out the project. There are a few ways of doing this. Cloning or Forking.

            #### Cloning
                A clone of a repository allows you to push and pull code to that same repository. If you are working for someone else you want to make a clone of the repository so you can push code to it.

            #### Forking
                A fork of a repository copies the repository and makes it your own.  Use this if you want to use it for your own purposes.

        ### Typical GIT lifecycle
            - Clone the repository if you don't already have it. When you do it will create a folder for the project in the folder you run the command from. The example will make a folder "fn_risk" with the repository in it.
                EXAMPLE: git clone https://gitlab.com/mardok_42/fn_risk.git

            - Checkout the development branch. This is not always called "development". But it is the branch that the changes are made to.
                EXAMPLE: git checkout develop

            - Check to be sure you are on the correct branch. This should list the branches. The current branch should have an "*" in front of it.
                EXAMPLE: get branch
                    OUTPUT:
                            * develop
                            master

            - Pull any new changes that may have been made.
                EXAMPLE: git pull origin develop

            - Checkout a new branch for the fix or feature off of the development branch. The last commands should ensure that we are on develop. The new branch will be made from the branch you are on. Most companies have a branch name standarization for doing so. Such as <typeOfBranch>/<TicketNumber>_<ShortDescription>. The "-b" flag creates a new branch. The type of branch is usually something like "feature" or "feat" for short. Or "bug" or "fix" for bug fixes.
                EXAMPLE: git checkout -b feat/RISK-123_HomePageUpdate

            -  Make your changes to the code. Save all your files.

            - Check what files have been changed
                EXAMPLE: git status

            - Add the files to be commited. You can do this one at a time or add all of them.
                EXAMPLE - One at a time: git add path/to/file.js
                EXAMPLE - All of them: git add .

            - Check to see that it is added:
                EXAMPLE: git status

            - Commit the files. There are many commit standardization. What we currently use at IHC is this format
                <typeOfBranch>(<TicketNUmber>): Message about the commit.
                EXAMPLE: git commit -m "feat(RISK-123): Updated home page content"

            - Check to see that it is commited:
                EXAMPLE: git status

            - Push to the server
                EXAMPLE: git push origin feat/RISK-123_HomePageUpdate

            - Make a pull request

            - Have the senior dev or code owner do a code review

            - Make any requested changes

            - Once the Pull Request(PR) is approved merge the code to develop.

            - checkout development branch

            - pull the changes to your develop
